BITBUCKET LINK: https://bitbucket.org/csten1/fit2096-assignment-2b/src/master/

What was done:
- Player movement with first person camera
	- rotY used to calculate the local forward to keep player movement constant.

- All enemy movement correctly
	- NOTE: enemy 2 will only move when the player is within a certain distance.
	- logic for condition if enemy dies (remove from the board) implemented.

- Sprites (additional)
	- Distance travelled (used in lab 6)
	- Bullets left (additional implementation)

- Bullets
	- Create vector of Bullet pointers.
	- Bullets fire once every time the spacebar is pressed. 

- Version Control
	- Link attached above.
	- Multiple versions of the solution is pushed incase something breaks the code.

What was attempted:
- Bullet travel
	- Firing the bullet works however it can not be seen travelling.
	- Update in Bullet.cpp has bugs would change/remove travelDist condition if collision was implemented.

What was not implemented:
Collision could not be completed in time
	- Remove travel dist in Bullet.cpp Update() function.
	- Change enemy1 movement to collide with player to end the game (remove if distanceToPlayer >= 9 statement).