#include "Player.h"

Player::Player(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, InputController* input, std::vector<Bullet*> inputBullets) :
GameObject(mesh, shader, texture, position)
{
	m_input = input;
	toRender = false; // turn off render so player won't have physical form 
	m_moveSpeed = 50.0f;
	m_turnSpeed = 3.0f;
	m_bullets = inputBullets;
	m_previousPosition = m_position;
}

void Player::Update(float timestep)
{
	// This is the first time a moving object in our game can rotate and change where
	// it's looking. Suddenly moving along the world axes is not useful to us anymore.
	// We need to work out what direction is forward for this particular object. 

	// We'll start by declaring a forward vector in world space
	Vector3 worldForward = Vector3(0, 0, 1);
	Vector3 worldRight = Vector3(1, 0, 0);

	// Next we'll wrap up our Y rotation in a matrix (remember matrices transform vectors)
	heading = Matrix::CreateRotationY(m_rotY);

	// Finally, we'll transform our world forward vector by the heading matrix which 
	// will essentially spin it from a world forward into a local forward which takes
	// the object's rotation into account.
	Vector3 localForward = Vector3::TransformNormal(worldForward, heading);
	Vector3 localRight = Vector3::TransformNormal(worldRight, heading);
	
	if (m_input->GetKeyHold('W'))
	{
		// Move along our local forward vector
		m_position += localForward * m_moveSpeed * timestep;
	}

	if (m_input->GetKeyHold('S'))
	{
		// Move along our local forward vector
		m_position += -localForward * m_moveSpeed * timestep;
	}

	if (m_input->GetKeyHold('A'))
	{
		m_position += -localRight * m_moveSpeed * timestep;
	}

	if (m_input->GetKeyHold('D'))
	{
		m_position += localRight * m_moveSpeed * timestep;
	}

	UpdateBullets();

	if (m_input->GetKeyUp(VK_SPACE)) // released one bullet each time space is pressed
	{
		for (unsigned int i = 0; i < m_bullets.size(); i++)
		{
			if (m_bullets[i]->GetIsActive() && !m_bullets[i]->GetPlayerActivate()) {
				m_bullets[i]->shoot(localForward);
				break;
			}
		}
	}

	m_distanceTravelled += Vector3::Distance(m_previousPosition, m_position);

	m_previousPosition = m_position;

}

void Player::UpdateBullets(){ // tracks the position of the player and updates the bullet position
	for (unsigned int i = 0; i < m_bullets.size(); i++)
	{
		m_bullets[i]->SetPosition(m_position + Vector3(0, 6, 0));
	}
}