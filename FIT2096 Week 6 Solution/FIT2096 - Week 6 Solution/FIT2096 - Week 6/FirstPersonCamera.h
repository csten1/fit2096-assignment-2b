#ifndef FIRST_PERSON_CAMERA_H
#define FIRST_PERSON_CAMERA_H

#include "Camera.h"
#include "GameObject.h"
#include "InputController.h"

class FirstPersonCamera : public Camera
{
private:
	GameObject* m_objectToFollow;
	Vector3 m_offset;

	bool m_catchupMode;
	float m_catchupSpeed;

	Vector3 TransformOffset();

	float m_heading;
	float m_pitch;

	InputController* m_input;

public:
	FirstPersonCamera();
	FirstPersonCamera(GameObject* target, Vector3 offset, bool catchup, float catchupSpeed, InputController* input);

	void Update(float timestep);
};

#endif