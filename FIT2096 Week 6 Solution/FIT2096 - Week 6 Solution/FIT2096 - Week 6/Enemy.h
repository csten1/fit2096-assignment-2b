#ifndef ENEMY_H
#define ENEMY_H

#include "GameObject.h"
#include "Player.h"

class Enemy : public GameObject
{
private:
	Player* player;
	int m_enemyNumber;
	// For distance travelled calculation
	int counter;
	Vector3 targetEnemy3;
	Vector3 targetEnemy5;
	bool enemy5moving;
	bool enemy4moving;

	float m_moveSpeed;
	float m_turnSpeed;
	int health;

public:
	Enemy(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, int newEnemyNumber, Player* playerToTrack);

	void Update(float timestep);
	void enemy1(float timestep);
	void enemy2(float timestep);
	void enemy3(float timestep);
	void enemy4(float timestep);
	void enemy5(float timestep);

};

#endif