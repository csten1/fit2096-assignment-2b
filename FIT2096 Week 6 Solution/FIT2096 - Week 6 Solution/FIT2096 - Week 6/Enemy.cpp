#include "Enemy.h"

Enemy::Enemy(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, int newEnemyNumber, Player* playerToTrack) :
	GameObject(mesh, shader, texture, position)
{
	SetUniformScale(5.0f);
	m_enemyNumber = newEnemyNumber;
	player = playerToTrack;

	m_moveSpeed = 50.0f;
	m_turnSpeed = 3.0f;

	health = 30;

	if (newEnemyNumber == 3) {
		targetEnemy3 = Vector3(rand() % 200 + (-100), 0, rand() % 200 + (-100));
	}

	if (newEnemyNumber == 4) {
		enemy4moving = false;
	}

	if (newEnemyNumber == 5) {
		targetEnemy5 = Vector3(0, 0, 0);
		enemy5moving = false;
	}
}

void Enemy::Update(float timestep)
{
	// directionToPlayer = the vector from the Enemy to the player
	// calculated using vector subtraction (see lecture notes for week 2)
	Vector3 directionToPlayer = player->GetPosition() - m_position;

	// Normalize the vector to get a vector of unit length 
	directionToPlayer.Normalize();

	// Calculate the angle the enemy should be facing
	// There are a couple of ways to do this, atan2 is fairly straightforward
	m_rotY = atan2(directionToPlayer.x, directionToPlayer.z);

	switch (m_enemyNumber) {
	case 1:
		enemy1(timestep);
		break;
	case 2:
		enemy2(timestep);
		break;
	case 3:
		enemy3(timestep);
		break;
	case 4:
		enemy4(timestep);
		break;
	case 5:
		enemy5(timestep);
		break;
	default:
		break;
	}
	
	if (health <= 0) {
		toRender = false;
	}
}

void Enemy::enemy1(float timestep) {
	if (player)
	{
		// Final minus initial
		Vector3 step = player->GetPosition() - m_position;
		step.Normalize();
		Vector3 toBePosition = m_position + step * m_moveSpeed * timestep;
		float distanceToPlayer = Vector3::Distance(toBePosition, player->GetPosition());

		if (distanceToPlayer >= 9) { // stops following the player at a certain distance so player can still see the enemy
			m_position += step * m_moveSpeed * timestep * 0.7;
		}
	}
}

void Enemy::enemy2(float timestep) {

	float playerDistance = Vector3::Distance(m_position, player->GetPosition());

	if (playerDistance < 50) { // only moves when player is near the enemy
		Vector3 playerToPosition = m_position - player->GetPosition();
		playerToPosition.Normalize();
		m_position += playerToPosition * m_moveSpeed * timestep;
	}
}

void Enemy::enemy3(float timestep) {
	Vector3 step = targetEnemy3 - m_position;
	step.Normalize();
	m_position += step * m_moveSpeed * timestep;
	float distanceToTarget = Vector3::Distance(m_position, targetEnemy3);

	if (distanceToTarget <= 0.1) { // when position is close enough to the target position
		targetEnemy3 = Vector3(rand() % 200 + (-100), 0, rand() % 200 + (-100)); // create new position for enemy to travel to
	}
}

void Enemy::enemy4(float timestep) {
	Vector3 infrontPlayer = player->GetPosition() + Vector3::TransformNormal(Vector3(0, 0, 25), player->GetHeading());
	Vector3 step = infrontPlayer - m_position;
	step.Normalize();
	Vector3 toBePos = m_position + step * m_moveSpeed * timestep;
	float distanceToTarget = Vector3::Distance(toBePos, infrontPlayer);
	
	if (enemy4moving) {
		m_position += step * m_moveSpeed * timestep * 0.5;
	}

	if (distanceToTarget <= 0.1) { // when position is close enough to the target position
		enemy4moving = false;
	}
	else {
		enemy4moving = true;
	}
}

void Enemy::enemy5(float timestep) {
	float playerDistance = Vector3::Distance(m_position, player->GetPosition());

	if (playerDistance < 30 && (!enemy5moving)) { // set random target position when player is near the enemy and if the enemy already reached it previous target
		targetEnemy5 = Vector3(rand() % 200 + (-100), 0, rand() % 200 + (-100));
		enemy5moving = true;
	}
	
	if (enemy5moving) {
		Vector3 step = targetEnemy5 - m_position;
		step.Normalize();
		m_position += step * m_moveSpeed * timestep;
		float distanceToTarget = Vector3::Distance(m_position, targetEnemy5);

		if (distanceToTarget <= 0.1) { // when position is close enough to the target position
			enemy5moving = false;
		}
	}
}