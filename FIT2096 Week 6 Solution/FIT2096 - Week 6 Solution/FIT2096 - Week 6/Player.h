#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"
#include "InputController.h"
#include "Bullet.h"
#include <vector>
class Player : public GameObject
{
private:
	InputController* m_input;
	std::vector<Bullet*> m_bullets;
	// For distance travelled calculation
	Vector3 m_previousPosition;
	float m_distanceTravelled;
	Matrix heading;

	float m_moveSpeed;
	float m_turnSpeed;

public:
	Player(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, InputController* input, std::vector<Bullet*> inputBullets);

	void Update(float timestep);
	void UpdateBullets();

	float GetDistanceTravelled() { return m_distanceTravelled; }
	Matrix GetHeading() { return heading; }
};

#endif