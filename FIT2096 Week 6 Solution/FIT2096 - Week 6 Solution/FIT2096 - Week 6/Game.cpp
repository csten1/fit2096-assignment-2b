/*	FIT2096 - Example Code
*	Game.cpp
*	Created by Elliott Wilson & Mike Yeates - 2016 - Monash University
*/

#include "Game.h"
#include "TexturedShader.h"
#include "SpinningObject.h"
#include "StaticObject.h"

#include "FlyingCamera.h"
#include "ThirdPersonCamera.h"
#include "FirstPersonCamera.h"

#include "DirectXTK/CommonStates.h"
#include <sstream>
#include <cstdlib>

Game::Game()
{
	m_renderer = NULL;
	m_currentCam = NULL;
	m_input = NULL;
	m_meshManager = NULL;
	m_textureManager = NULL;
	m_diffuseTexturedShader = NULL;
	m_unlitVertexColouredShader = NULL;
	m_spriteBatch = NULL;
	m_arialFont12 = NULL;
	m_arialFont18 = NULL;
	m_player = NULL;
}

Game::~Game() {}

bool Game::Initialise(Direct3D* renderer, InputController* input)
{
	m_renderer = renderer;	
	m_input = input;
	m_meshManager = new MeshManager();
	m_textureManager = new TextureManager();

	if (!InitShaders())
		return false;

	if (!LoadMeshes())
		return false;

	if (!LoadTextures())
		return false;

	LoadFonts();
	InitUI();
	InitGameWorld();
	RefreshUI();

	m_currentCam = new FirstPersonCamera(m_player, Vector3(0, 10, -25), true, 2.0f, m_input);
	return true;
}

bool Game::InitShaders()
{
	m_unlitVertexColouredShader = new Shader();
	if (!m_unlitVertexColouredShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/VertexColourPixelShader.ps"))
		return false;

	m_diffuseTexturedShader = new TexturedShader();
	if (!m_diffuseTexturedShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/TexturedPixelShader.ps"))
		return false;

	return true;
}

bool Game::LoadMeshes()
{
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/ground.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/wall.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/rumble_strip.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/enemy.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/player_capsule.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/bullet.obj"))
		return false;

	return true;
}

bool Game::LoadTextures()
{
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/grass.jpg"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/rumble_strip.jpg"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/wall.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_white.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_red.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/bullet.png"))
		return false;

	return true;
}

void Game::LoadFonts()
{
	// There's a few different size fonts in there, you know
	m_arialFont12 = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-12pt.spritefont");
	m_arialFont18 = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-18pt.spritefont");
}

void Game::InitUI()
{
	m_spriteBatch = new SpriteBatch(m_renderer->GetDeviceContext());
}


void Game::RefreshUI()
{
	// Ensure text in UI matches latest scores etc (call this after data changes)
	// Concatenate data into our label string using a wide string stream
	if (m_player)
	{
		std::wstringstream ss;

		// Round to two decimal places for neater output
		ss << "Distance Travelled: " << floorf(m_player->GetDistanceTravelled() * 100) / 100;

		m_distanceTravelledText = ss.str();
	}

	bulletsLeft = 0;
	for (unsigned int i = 0; i < m_bullets.size(); i++)
	{
		if (m_bullets[i]->GetIsActive() && !m_bullets[i]->GetPlayerActivate()) {
			bulletsLeft += 1;
		}
	}

	std::wstringstream ss1;

	ss1 << "Bullets Left: " << bulletsLeft;

	m_bulletsLeftText = ss1.str();
}

void Game::InitGameWorld()
{
	bulletsLeft = 0;
	for (int i = 0; i <= 20; i++) {
		m_bullets.push_back(new Bullet(m_meshManager->GetMesh("Assets/Meshes/bullet.obj"),
			m_diffuseTexturedShader,
			m_textureManager->GetTexture("Assets/Textures/bullet.png"),
			Vector3(0, 0, 0)));
		bulletsLeft += 1;
	}

	m_player = new Player(m_meshManager->GetMesh("Assets/Meshes/player_capsule.obj"),
						  m_diffuseTexturedShader,
						  m_textureManager->GetTexture("Assets/Textures/tile_white.png"),
						  Vector3(0, 0, 0),
						  m_input,
						  m_bullets);

	m_gameObjects.push_back(m_player);

	for (int i = 1; i < 6; i++) {
		m_gameObjects.push_back(new Enemy(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"), 
										  m_diffuseTexturedShader, 
										  m_textureManager->GetTexture("Assets/Textures/tile_red.png"), 
										  Vector3(rand() % 200 + (-100), 0, rand() % 200 + (-100)), i, m_player));
	}

	// Static scenery objects
	m_gameObjects.push_back(new StaticObject(m_meshManager->GetMesh("Assets/Meshes/ground.obj"),
		m_diffuseTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/grass.jpg")));

	m_gameObjects.push_back(new StaticObject(m_meshManager->GetMesh("Assets/Meshes/wall.obj"),
		m_diffuseTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/wall.png")));

	m_gameObjects.push_back(new StaticObject(m_meshManager->GetMesh("Assets/Meshes/rumble_strip.obj"),
		m_diffuseTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/rumble_strip.jpg")));

}

void Game::Update(float timestep)
{
	m_input->BeginUpdate();
	
	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->Update(timestep);
	}

	bulletsLeft = 0;
	for (unsigned int i = 0; i < m_bullets.size(); i++)
	{
		if (m_bullets[i]->GetIsActive() && !m_bullets[i]->GetPlayerActivate()) {
			//m_bullets[i]->Update(timestep);
			bulletsLeft += 1;
		}
	}
	// We're refreshing our UI every frame as we're assuming the distance travelled text will be changing frequently
	// Usually our UI will only need refreshing when a piece of data we're displaying changes
	RefreshUI();
	
	m_currentCam->Update(timestep);

	m_input->EndUpdate();
}

void Game::Render()
{
	m_renderer->BeginScene(0.8f, 1.0f, 0.9f, 1.0f);

	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		if (m_gameObjects[i]->GetToRender()) { // checks if the current game object should render or not
			m_gameObjects[i]->Render(m_renderer, m_currentCam);
		}
	}

	for (unsigned int i = 0; i < m_bullets.size(); i++)
	{
		if (m_bullets[i]->GetToRender()) { // checks if the current game object should render or not
			m_bullets[i]->Render(m_renderer, m_currentCam);
		}
	}

	DrawUI();

	m_renderer->EndScene();		
}

void Game::DrawUI()
{
	// Sprites don't use a shader 
	m_renderer->SetCurrentShader(NULL);

	CommonStates states(m_renderer->GetDevice());
	m_spriteBatch->Begin(SpriteSortMode_Deferred, states.NonPremultiplied());

	// Do UI drawing between the Begin and End calls

	// Let's draw some text over our game
	m_arialFont18->DrawString(m_spriteBatch, m_distanceTravelledText.c_str(), Vector2(500, 680), Color(1.0f, 1.0f, 1.0f), 0, Vector2(0,0));
	m_arialFont18->DrawString(m_spriteBatch, m_bulletsLeftText.c_str(), Vector2(500, 630), Color(1.0f, 1.0f, 1.0f), 0, Vector2(0, 0));
	
	m_spriteBatch->End();
}

void Game::Shutdown()
{
	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		delete m_gameObjects[i];
	}

	m_gameObjects.empty();

	if (m_currentCam)
	{
		delete m_currentCam;
		m_currentCam = NULL;
	}

	if (m_unlitVertexColouredShader)
	{
		m_unlitVertexColouredShader->Release();
		delete m_unlitVertexColouredShader;
		m_unlitVertexColouredShader = NULL;
	}

	if(m_diffuseTexturedShader)
	{
		m_diffuseTexturedShader->Release();
		delete m_diffuseTexturedShader;
		m_diffuseTexturedShader = NULL;
	}

	if (m_meshManager)
	{
		m_meshManager->Release();
		delete m_meshManager;
		m_meshManager = NULL;
	}

	if (m_textureManager)
	{
		m_textureManager->Release();
		delete m_textureManager;
		m_textureManager = NULL;
	}

	if (m_spriteBatch)
	{
		delete m_spriteBatch;
		m_spriteBatch = NULL;
	}

	if (m_arialFont12)
	{
		delete m_arialFont12;
		m_arialFont12 = NULL;
	}

	if (m_arialFont18)
	{
		delete m_arialFont18;
		m_arialFont18 = NULL;
	}


}