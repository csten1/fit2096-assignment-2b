#include "FirstPersonCamera.h"
#include "MathsHelper.h"
FirstPersonCamera::FirstPersonCamera()
{
	m_catchupMode = false;
	m_catchupSpeed = 1.0f;
	m_objectToFollow = NULL;
	m_offset = Vector3(0.0f, 10.0f, -10.0f);

	SetPosition(m_objectToFollow->GetPosition() + TransformOffset());
}

FirstPersonCamera::FirstPersonCamera(GameObject* target, Vector3 offset, bool catchup, float catchupSpeed, InputController* input)
{
	m_catchupMode = catchup;
	m_catchupSpeed = catchupSpeed;
	m_objectToFollow = target;
	m_offset = offset;

	m_input = input;
	m_heading = 0.0f;
	m_pitch = 0.0f;

	// Set starting position so catchup mode doesn't show camera animating from the origin
	SetPosition(m_objectToFollow->GetPosition() + TransformOffset());
}

void FirstPersonCamera::Update(float timestep)
{
	if (m_objectToFollow != NULL)
	{
		// Accumulate change in mouse position 
		m_heading += m_input->GetMouseDeltaX() * 2.0f * timestep;
		m_pitch += m_input->GetMouseDeltaY() * 2.0f * timestep;
		m_pitch = MathsHelper::Clamp(m_pitch, ToRadians(-80.0f), ToRadians(80.0f));

		// Wrap heading and pitch up in a matrix so we can transform our look at vector
		// Heading is controlled by MouseX (horizontal movement) but it is a rotation around Y
		// Pitch  controlled by MouseY (vertical movement) but it is a rotation around X
		Matrix heading = Matrix::CreateRotationY(m_heading);
		m_objectToFollow->SetYRotation(m_heading);
		Matrix pitch = Matrix::CreateRotationX(m_pitch);

		// Not storing this directly in m_offset as it would accumulate over time and spin out of control
		Vector3 rotatedOffset = TransformOffset();

		// Combine pitch and heading into one matrix for convenience
		Matrix lookAtRotation = pitch * heading;

		// Transform a world forward vector into local space (take pitch and heading into account)
		Vector3 lookAt = Vector3::TransformNormal(Vector3(0, 0, 1), lookAtRotation);

		// At this point, our look-at vector is still relative to the origin
		// Add our position to it so it originates from the camera and points slightly in front of it
		// Remember the look-at vector needs to describe a point in the world relative to the origin
		lookAt += (m_objectToFollow->GetPosition());

		// Use parent's mutators so isDirty flags get flipped
		SetLookAt(lookAt + Vector3(0, 7, 0));

		if (m_catchupMode)
		{
			// Catchup to target position. A simple LERP will do.
			SetPosition(m_objectToFollow->GetPosition() + Vector3(0, 7, 0));
		}
		else
		{
			// Jump directly to target position (including offset)
			SetPosition(m_objectToFollow->GetPosition() + Vector3(0, 7, 0));
		}
	}

	// Give our parent a chance to regenerate the view matrix
	Camera::Update(timestep);
}

Vector3 FirstPersonCamera::TransformOffset()
{
	// Wrap up the Y rotation of the object we're following in a matrix
	Matrix rotation = Matrix::CreateRotationY(m_objectToFollow->GetYRotation());

	// Transform our offset vector by the rotation so the offset rotates with our follow object
	return Vector3::TransformNormal(m_offset, rotation);
}