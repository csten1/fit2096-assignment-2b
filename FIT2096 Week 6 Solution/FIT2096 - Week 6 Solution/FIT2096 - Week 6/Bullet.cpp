#include "Bullet.h"

Bullet::Bullet(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position) : GameObject(mesh, shader, texture, position) {
	active = true;
	toRender = true;
	travelDist = 0;
	playerActivate = false;
}

Bullet::~Bullet() {}

void Bullet::shoot(Vector3 direction) {
	forward = direction;
	playerActivate = true;
}

void Bullet::Update(float timestep) {

	forward.Normalize();
	if (travelDist < 100) {
		m_position += forward * 0.5 * timestep;
		travelDist++;
	}
	else {
		active = false;
		toRender = false;
	}
}