#ifndef BULLET_H
#define BULLET_H

#include "GameObject.h"

class Bullet : public GameObject {
private:
	bool active;
	int travelDist;
	Vector3 forward;
	bool playerActivate;

public:
	Bullet(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position);
	~Bullet();

	void Update(float timestep);
	bool GetIsActive() { return active; }
	bool GetPlayerActivate() { return playerActivate; }
	void shoot(Vector3 direction);
};

#endif // !BULLET_H
